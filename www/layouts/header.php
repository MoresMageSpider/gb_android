<?php include 'layouts/head.php'?>
<div class="app">
	<header class="header bg-white">

		<div class="header-container align-center" id="header-main-group">
			<button class="nav-button place-left" id="toggleSideMenu"><img src="assets/img/more@3x.png"> </button>
			<span class="app-title app-logo"><a href=""><img src="assets/img/gordon_biersch.png"/></a> </span>
			<button class="tool-button place-right  bg-white" id="searchButton" style="visibility: hidden"><span
					class="i-search icon fg-violet"></span></button>
		</div>

		<?php include 'layouts/menu.php' ?>

	</header>
