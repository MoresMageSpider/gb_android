<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="viewport-fit=cover, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">

	<title>GB LIFE</title>
	<link rel="stylesheet" href="vendors/coreCss/css/core-colors.min.css">
	<link rel="stylesheet" href="vendors/coreCss/css/core-icons.min.css">
	<link rel="stylesheet" href="vendors/coreCss/css/core.min.css">
	<link rel="stylesheet" href="assets/css/style.css">

	<script src="vendors/jQuery/jquery-1.12.4.min.js"></script>
    <script src="assets/js/masonry.pkgd.min.js"></script>
	<script src="vendors/coreCss/js/core.min.js"></script>
    <script src="vendors/jsbn.js"></script>
    <script src="vendors/rsa.js"></script>
	<!--<script src="phonegap.js"></script>-->
	<title>gBApp</title>

    <script>
        var parseQueryString = function(url) {
            var urlParams = {};
            url.replace(
                new RegExp("([^?=&]+)(=([^&]*))?", "g"),
                function($0, $1, $2, $3) {
                    urlParams[$1] = $3;
                }
            );

            return urlParams;
        },
        options = { enableHighAccuracy: true };

        function onError(error){
            alert(error.code + error.message);
        }

    </script>


    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-125098099-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-125098099-1');
    </script>

</head>
<body>
<div class="ajax-loading" style="display: none">
    <div class="flex-center full-screen">
        <div data-role="progress"
             data-type="circle"
             data-size="32"
             data-radius="10"
             data-bar-color="st-red"></div>
    </div>
</div>