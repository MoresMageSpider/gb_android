
</div>
<script>

    $('#searchButton').on('click', function () {
        var b = $(this);

        if (b.attr('id') == 'searchButton') {
            $("#header-search-group").fadeIn('fast');

            $('header .marker, header .search span').hide(100);

            $('.news-search').addClass('align-left');

            $('.search').animate({
                width: '100%',
                margin: 0
            }, 300);

            $(".search-in-list").focus();
        }
    });


    $('#toggleSideMenu').click(function () {
        $(this).toggleClass('transform');
        $('#header-menu-group').toggleClass('active');
    });




    $(document).bind("ajaxSend", function(){
        $('.ajax-loading').fadeIn('fast');
    }).bind("ajaxComplete", function(){
        $('.ajax-loading').fadeOut('fast');
    });

    $('.nav-back').click(function(e) {
        e.preventDefault();
        window.history.back();
    });

    jQuery(document).ready(function($){
        $('#header-menu-group').css({height: window.innerHeight})
    });


    jQuery(document).ready(function($){
        if(typeof pageTitle !== undefined) $('#header-main-group span.fg-black').text(pageTitle);
    })

</script>
</body>
</html>