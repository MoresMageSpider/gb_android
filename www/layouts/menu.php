<div class="sidebar-header bg-white" id="header-menu-group">
	<ul class="sidebar-menu">
		<li class="line-bottom nearlyBranch"><a href="#"><span><img src="assets/img/gpslocation@2x.png"/></span><span id="nearlyBranch"> 台北信義店<br>02-87867588</span></a>
		</li>
		<li><a href="login-2.html"><span><img src="assets/img/homepage@2x.png"/></span><span>首頁</span></a>
		</li>

		<li><a href="igb_member_description.html#direct"><span><img src="assets/img/igbmember@2x.png"/></span><span>iGB會員</span></a></li>
		<li><a href="news.html"><span class="news-notify"><img src="assets/img/eventhome@2x.png"/><span></span></span><span>活動訊息</span></a>
		</li>
		<li><a href="search.html"><span><img src="assets/img/gpslocation@2x.png"/></span><span>門市查詢</span></a></li>
		<li class="line-bottom"><a href="3.html" class="checkMenu"><span><img src="assets/img/menu@2x.png"/></span><span>查看菜單</span></a>
		</li>
		<li><a href="#"><span><img src="assets/img/igbabout@2x.png"/></span><span>GBA會員說明</span></a></li>
		<li><a href="#"><span><img src="assets/img/recieptrestore@2x.png"/></span><span>帳單補登</span></a></li>
		<li><a href="giftcard_search.html"><span><img src="assets/img/giftcard@2x.png"/></span><span>禮物卡查詢</span></a></li>
		<li><a href="#"><span><img src="assets/img/language@2x.png"/></span><span>語系</span></a></li>
		<li><a href="#"><span><img src="assets/img/notification@2x.png"/></span><span>訊息通知</span>
				<label class="switch">
					<input type="checkbox" [checked, disabled]>
					<span class="check bg-violet"></span>
				</label>
			</a></li>
		<li><a href="#" id="openStore"><span><img src="assets/img/rank@2x.png"/></span><span>APP評分</span></a></li>
		<li><a href="faq.html"><span><img src="assets/img/qa@2x.png"/></span><span>常見問題</span></a></li>
		<li><a href="https://www.facebook.com/GB%E9%AE%AE%E9%87%80%E9%A4%90%E5%BB%B3-159579000757549/" openinapp><span><img src="assets/img/fb@2x.png"/></span><span>Facebook粉絲團</span></a></li>
		<li><a href="https://youtu.be/Y-FYL-M6zGM" target="blank"><span><img src="assets/img/yt@2x.png"/></span><span>觀看Youtube</span></a></li>
		<li><a href="#"><span><img src="assets/img/logout@2x.png"/></span><span>登出</span></a></li>

	</ul>
</div>