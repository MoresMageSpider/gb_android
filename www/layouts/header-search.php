<?php include 'layouts/head.php' ?>
<div class="app">
    <header class="header bg-white shadow-0">


        <div class="header-container align-center" id="header-main-group">
            <a href="#" class="nav-back"><img src="assets/img/page1@2x.png"></a> </a>
            <span class="fg-black">門市查詢</span>
            <button class="place-right  bg-white" style="visibility: hidden"><span
                        class="i-search icon fg-violet"></span></button>
        </div>


        <div class="header-container news-search pos-absolute full-width flex-justify">

            <div class="marker no-overflow"><img src="assets/img/nearestStore@2x.png"/> <span>鄰近門市</span></div>

            <div class="search" id="searchButton">
                <a href="#"><img src="assets/img/searchwhite@2x.png"> </a> <span>搜尋門市</span>
            </div>


        </div>

        <div class="header-container pos-absolute flex-center" id="header-search-group" style="display: none;">
            <input type="text" name="search" class="search-in-list fg-white">
        </div>

    </header>