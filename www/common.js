if (window.cordova) {
    document.addEventListener("deviceready", function() {
        if (window.FirebasePlugin) {
            window.FirebasePlugin.grantPermission();
            window.FirebasePlugin.getToken(function(token) {
                // save this server-side and use it to push notifications to this device
                localStorage.deviceToken = (token);
            }, function(error) {
                console.error(error);
            });
            window.FirebasePlugin.onMessageReceived(function(notification) {
                if (notification.tap) {
                    console.log(notification)
                    if(notification.tag =='news'){
                        location.href = "news-detail.html?id="+notification.GB_data;
                    }
                    else if(notification.tag =='coupon'){
                        console.log(localStorage)
                        $.post("https://www.gbarestaurants.com.tw/icms/api/GBapi_20/getVouchers",{
                            lang: 'zh',
                            type: 'usable',
                            token: localStorage.token
                        },function(res){
                            console.log(res);
                            for (var i = 0; i < res.data.length; i++) {
                                if (res.data[i].coupon_content_id == notification.GB_id) {
                                    var ticket = (res.data[i]);
                                    console.log(ticket);
                                    localStorage.couponDetails= JSON.stringify(ticket);
                                }
                            }
                            location.href = "coupon-details.html?ticketId="+ticket.ticketId+"&type=usable&source=iGB";
                        });
                    }
                    else
                    {
                        location.href = "index.html";
                    }
                } else {
                    // location.href = "news-detail.html?id="+notification.GB_data;
                }
            }, function(error) {
                console.log(error);
            });

            $.post("https://www.gbarestaurants.com.tw/icms/api/GBapi_20/getNotReadList", {
                token: localStorage.token,
                lang: localStorage.lang,
            }, function(res) {
                var count = 0;

                for(var i = 0;i < res.data.length;i++){
                    if(res.data[i].isRead == 0){
                        count++;
                    }   
                }
                window.FirebasePlugin.setBadgeNumber(count);
            });
        }
        console.log("window.device --------------- ");
        console.log(window.device);
        // if(window.device.model.indexOf("iPhone10") >= 0) {
        //     $("#header-menu-group").css("top", "25px");
        //     $("#header-menu-group").css("height", ($("#header-menu-group").height()+25)+"px");
        // }
        if (window.device.platform == "Android") {
            $("#openStore").attr('href', "market://details?id=com.gb.com.gb.app");
        } else {
            $("#openStore").attr('href', "itms://itunes.apple.com/us/app/gb-life/id1403309946");
        }
    }, false);
}


$(document).ready(function() {
    $("#toggleSideMenu").css("padding-left", "0");
    if (localStorage.locales) {
        var locales = JSON.parse(localStorage.locale);
        for (var text in locales) {
            $('.txt_' + text).html(locales[text]);
            // if (localStorage.lang == "zh") {
            //     if (text == "iGB_Member") {
            //         $('.txt_' + text).html("iGB會員專區");
            //     }
            //     if (text == "iGB_Description") {
            //         // $('.txt_' + text).html("iGB說明");
            //         $('.txt_' + text).html("iGB會員卡說明");
            //     }
            // }
            // if (localStorage.lang == "en") {
            //     if (text == "News") {
            //         // $('.txt_' + text).html("iGB說明");
            //         $('.txt_' + text).html("What's News");
            //     }
            //     if (text == "Booking") {
            //         // $('.txt_' + text).html("iGB說明");
            //         $('.txt_' + text).html("Restaurant Finder");
            //     }
            //     if (text == "Bill_Record") {
            //         // $('.txt_' + text).html("iGB說明");
            //         $('.txt_' + text).html("Retroactive Bill");
            //     }
            //     if (text == "App_Grade") {
            //         // $('.txt_' + text).html("iGB說明");
            //         // $('.txt_' + text).html("App Ratings");
            //     }
            // }
        }
    }
    
    $('[openinapp]').click(function(e) {
        e.preventDefault();
        cordova.InAppBrowser.open($(this).attr('href'), '_blank', 'location=yes,clearcache=no,hardwareback=yes');
    });


    if (localStorage.showNotification == "1" && $("#notify_switch").length) {
        $("#notify_switch")[0].checked = true;
        $(".news-notify span").removeClass("off");
    } else if ($("#notify_switch").length) {
        $("#notify_switch")[0].checked = false;
        $(".news-notify span").addClass("off");
    }

    $("#notify_switch").change(function(e) {
        if(this.checked) {
            $.post("https://www.gbarestaurants.com.tw/icms/api/GBapi_20/setNotificationSwitch", {
                token: localStorage.token,
                notification: 1
            }, function(res) {
                // console.log(res);
            });
        } else {
            $.post("https://www.gbarestaurants.com.tw/icms/api/GBapi_20/setNotificationSwitch", {
                token: localStorage.token,
                notification: 0
            }, function(res) {
                // console.log(res);
            });
        }
    });

    if (localStorage.lang == "en") {
        $('#nearlyBranch').html("No Positioning! Please turn on the position function");
    }
    
    if (localStorage.locations && localStorage.nearlybranch) {
        // console.log("wow");
        $(JSON.parse(localStorage.locations).list).each(function(i, o) {
            if (o.code === localStorage.nearlybranch) {
                $('#nearlyBranch').html(this.name + '<br>' + this.phone);
                $('.nearlyBranch').find('a').attr('href', 'branch-detail.html?code=' + o.code);
                return;
            }
        });
    }

    $.post("https://www.gbarestaurants.com.tw/icms/api/GBapi_20/getNewsNotReadCount", {
        token: localStorage.token,
    }, function(res) {
        // console.log(res);

        $(".badge2").show().html(res.data);
        if (res.data == 0) { $(".badge2").hide(); }

        $(".news-notify-count span").show().html(res.data).css('display', 'flex');
        if (res.data == 0) { $(".news-notify-count span").hide(); }
    });
    
    $.post("https://www.gbarestaurants.com.tw/icms/api/GBapi_20/getNotReadList", {
        token: localStorage.token,
        lang: localStorage.lang,
        // type: '1'
    }, function(res) {
        // console.log(res.data);
        var count = 0;

        for(var i = 0;i < res.data.length;i++){
            // console.log(res.data[i]);
            if(res.data[i].isRead == 0){
                // console.log(res.data[i]);
                count++;
                // console.log(count);
            }
        }

        $(".news-notify span").show().html(count);
        if (count == 0) { $(".news-notify span").hide(); }

    });

    // $.post("https://www.gbarestaurants.com.tw/icms/api/GBapi_20/getNotReadCount", {
    //     token: localStorage.token,
    //     // lang: localStorage.lang,
    //     // type: '1'
    // }, function(res) {
    //     // console.log(res);
    //     $(".news-notify span").show().html(res.data);
    //     if (res.data == 0) { $(".news-notify span").hide(); }

    //     $(".news-notify-count span").show().html(res.data).css('display', 'flex');
    //     if (res.data == 0) { $(".news-notify-count span").hide(); }
    // });

    // $.post("https://www.gbarestaurants.com.tw/icms/api/GBapi_20/getNewsNotReadCount", {
    //     token: localStorage.token
    // }, function(res) {
    //     // console.log(res);
    //     $(".news-notify-count span").show().html(res.data).css('display', 'flex');
    //     if (res.data == 0) { $(".news-notify-count span").hide(); }
    // });

    $.post("https://www.gbarestaurants.com.tw/icms/api/GBapi_20/getNewsNotReadList", {
        token: localStorage.token,
        lang: localStorage.lang
    }, function(res) {
        // console.log(res);
    });

    if (!localStorage.token) {
        $(".txt_Logout").parent().parent().hide();
    }
});

function runAgain(){
    if (localStorage.lang == "en") {
        $('#nearlyBranch').html("No Positioning! Please turn on the position function");
    }
    if (localStorage.locations && localStorage.nearlybranch) {
        // console.log("wow");
        $(JSON.parse(localStorage.locations).list).each(function(i, o) {
            if (o.code === localStorage.nearlybranch) {
                $('#nearlyBranch').html(this.name + '<br>' + this.phone);
                $('.nearlyBranch').find('a').attr('href', 'branch-detail.html?code=' + o.code);
                return;
            }
        });
    }
}

function logout() {
    $.post("https://www.gbarestaurants.com.tw/icms/api/GBapi_20/Logout", {
        token: localStorage.token
    }, function(res) {
        if (res) {
            localStorage.removeItem('token');
            location.href = "2.html";
        }
    });
}
