Lang.prototype.pack.en = {
	"token": {
		"訂位":"Reservation",
		"大人":"Adult(s)",
		"小孩":"Kid(s)",
		"4人以上的訂位，請使用電話預約":"For reservations of more than 4 people, please use the telephone to book",
		"訂位日期":"Date",
		"訂位時段":"Available times",
		"下一步":"Next",
		"午餐":"Lunch",
		"晚餐":"Dinner",
		"人數":"Party size",
		"大":"A",
		"小":"K",
		"訂位人姓名*":"Reservation Name*",
		"ex: 王小明":"ex: Xiaoming Wang",
		"先生":"male",
		"小姐":"female",
		"訂位人電話*(訂位完成會寄送簡訊通知)":"Mobile phone number*-sms confirmation will be received",
		"用餐目的":"Special event",
		"慶生":"Birthday",
		"約會":"Date",
		"週年慶":"Anniversary",
		"家庭用餐":"Family",
		"朋友聚餐":"Friends",
		"商務聚餐":"Business",
		"訂位備註(選填)":"Comment(optional)",
		"有任何特殊需求嗎？可以先填寫在這裡喔！（例如：行動不便、過敏)":"There is any need to fill in here.\nFor example: mobility, allergies",
		"特殊需求":"Special Request",
		"安排兒童椅":"Number of high chairs",
		"張":" ",
		"*為必填":"*required",
		"完成訂位":"Submit"
	},
	"regex": [
		
	]
};