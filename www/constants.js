/*	booking constants*/
const BookingConstants = {
	"bookingBaseUrl":"https://api.inlineapps.com/reservations/-KpZYK_7r8Cymof4xnZN",
	"branchIDMap" : {
		"sinyi" : "-KrUcxEi8rWRvWwZssL9",
		"dunhuan" : "-KrUcxDEGdGov1kxPCwA",
		"nancy" : "-KrUdQFBddOxe7p6yiT5",
		"linco" : "-KrUcxDB3ZPhOGcwoFos",
		"hsinkun" : "-KrUcxD6seLOVaOxoWfo",
		"dazi" : "-LRoWJ-t2ug-4hE53dW9",
		"zhubei" : "-M2m5A81f8jqmrpiumMz"

	},
	"X_API_Key": "hyCisfo9zACAYmFvWXiUTCLW9@T{LFe9W8LX23Ud"
}

/*	Link auto fix for news detail on mobile */
const NewsDetailConstants = {
	"baseUrl": "https://www.gbarestaurants.com.tw", 
	"autoFixLinkMap": {
		"/zh/igb/support/": "igb_member_description.html",
		"/zh/igb/account": "card.html",
		"/zh/giftcard": "giftcard_search.html"
	}
}