define({ "api": [
  {
    "type": "POST",
    "url": "GBapi_20/checkTokenStatus",
    "title": "檢查Token狀態",
    "version": "0.2.0",
    "name": "checkTokenStatus",
    "group": "Auth",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登入token</p>"
          }
        ]
      }
    },
    "description": "<p>檢查Token狀態。</p>",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi_20.php",
    "groupTitle": "Auth",
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": true,\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "AuthTokenExpired Response",
          "content": "HTTP/1.1 200 Bad Request\n{\n    \"success\": false,\n    \"code\": \"E01\",\n    \"message\": \"Token Expired\",\n    \"error\": \"token fail\"\n}",
          "type": "json"
        },
        {
          "title": "AuthTokenFail Response",
          "content": "HTTP/1.1 200 Bad Request\n{\n    \"success\": false,\n    \"code\": \"E02\",\n    \"message\": \"Token Error\",\n    \"error\": \"token fail\"\n}",
          "type": "json"
        },
        {
          "title": "APIError Response",
          "content": "HTTP/1.1 200 Bad Request\n{\n    \"success\": false,\n    \"error\": \"input error, please check again\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "POST",
    "url": "GBapi_20/refreshToken",
    "title": "更新Token",
    "version": "0.2.0",
    "name": "refreshToken",
    "group": "Auth",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登入token</p>"
          }
        ]
      }
    },
    "description": "<p>更新Token，延展期限90天。</p>",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi_20.php",
    "groupTitle": "Auth",
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": true,\n    \"token\": \"ff207f17-05eb-3ae4-2ae5-2a6d0b120ef8\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "AuthTokenError Response",
          "content": "HTTP/1.1 200 Bad Request\n{\n    \"success\": false,\n    \"error\": \"Token error or expired\"\n}",
          "type": "json"
        },
        {
          "title": "APIError Response",
          "content": "HTTP/1.1 200 Bad Request\n{\n    \"success\": false,\n    \"error\": \"input error, please check again\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "POST",
    "url": "GBapi/AppCoupon",
    "title": "取得優惠列表（for app）",
    "version": "0.1.0",
    "name": "AppCoupon",
    "group": "Company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>語系 (zh 中文, en 英文)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lat",
            "description": "<p>使用者經度</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lng",
            "description": "<p>使用者緯度</p>"
          }
        ]
      }
    },
    "description": "<p>取得優惠列表。</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "subTitle",
            "description": "<p>優惠兌換展開後，最上面顯示的抬頭。</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": {\n        \"subTitle\": \"iGB PRIMIER AWARD REDEMPTION\",\n        \"list\": [\n            {\n                \"coupon_id\": \"1\",\n                \"point\": \"3000點 兌換選項\",\n                \"sort_index\": \"0\",\n                \"detail\": [\n                    {\n                        \"image_id\": \"6\",\n                        \"image_url\": \"https://www.gbarestaurants.com.tw/icms/uploads/webs/app_coupons/68a13035-b2c9-76b1-1a2c-544f439928b6_a.jpg\",\n                        \"description\": \"多特蒙黃金拉格 GOLDEN EXPORT <br />  小麥酵母愛爾 HEFEWEIZEN <br />  捷克皮爾森 CZECH PILSNER <br />  梅爾森拉格 MÄRZEN <br />  史瓦茲黑啤酒 SCHWARZBIER <br />  季節啤酒 Seasonal\"\n                    },\n                    {\n                        \"image_id\": \"7\",\n                        \"image_url\": \"https://www.gbarestaurants.com.tw/icms/uploads/webs/app_coupons/7ad44a59-b309-800d-4b93-372cfab6ce12_a.jpg\",\n                        \"description\": \"紅白酒系列單杯飲品 <br />  Red&White<br />  波本密桃2.0 <br />  Bourbon Peach Smash<br />  草莓檸檬汁 2杯 <br />  2 Cups of Strawberry Lemonade\"\n                    }\n                ]\n            },\n            {\n                \"coupon_id\": \"2\",\n                \"point\": \"5000點 兌換選項\",\n                \"sort_index\": \"1\",\n                \"detail\": [\n                    {\n                        \"image_id\": \"8\",\n                        \"image_url\": \"https://www.gbarestaurants.com.tw/icms/uploads/webs/app_coupons/cf0df1f2-26f8-6a34-908e-df1083960e7c_a.jpg\",\n                        \"description\": \"哈維斯沙拉<br />  Harvest\"\n                    },\n                    {\n                        \"image_id\": \"9\",\n                        \"image_url\": \"https://www.gbarestaurants.com.tw/icms/uploads/webs/app_coupons/0a019e8c-e247-5dd1-ae5f-d20f251f7913_a.jpg\",\n                        \"description\": \"泰式香辣雞翅<br />  Thai Fire Chicken Wings<br />  瑞所託菇菇起司球<br />  Wund Mushroom Risotto Fritters \"\n                    }\n                ]\n            },\n            {\n                \"coupon_id\": \"4\",\n                \"point\": \"6000點 兌換選項\",\n                \"sort_index\": \"2\",\n                \"detail\": []\n            }\n        ]\n      }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "Company"
  },
  {
    "type": "POST",
    "url": "GBapi/AppMenu",
    "title": "取得菜單資訊（for app）",
    "version": "0.1.0",
    "name": "AppMenu",
    "group": "Company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>語系 (zh 中文, en 英文)</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "branch",
            "description": "<p>分店Code</p>"
          }
        ]
      }
    },
    "description": "<p>取得列表資訊。</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "weight",
            "description": "<p>依此資料做圖片順序。</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": [\n            {\n                \"item_id\": \"1\",\n                \"item_title\": \"主餐單供應時間: 週日~週六 11:00~24:00\",\n                \"sort_index\": \"1\",\n                \"images\": {\n                    \"image_url\": \"https://www.gbarestaurants.com.tw/icms/uploads/webs/app_items/1134e310-0216-c490-85a1-5867986e2c39_a.jpg\",\n                    \"image_id\": \"5\",\n                    \"type\": \"app_items\",\n                    \"weight\": \"1518078775\"\n                }\n            },\n            {\n                \"item_id\": \"2\",\n                \"item_title\": \"商業午餐: 週一~週五 11:00~14:00\",\n                \"sort_index\": \"2\",\n                \"images\": {\n                    \"image_url\": \"https://www.gbarestaurants.com.tw/icms/uploads/webs/app_items/094df1b8-a3b0-d6b3-a10c-d86e1df9e994_a.jpg\",\n                    \"image_id\": \"3\",\n                    \"type\": \"app_items\",\n                    \"weight\": \"1518078722\"\n                }\n            }\n        ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "Company"
  },
  {
    "type": "POST",
    "url": "GBapi/Branch",
    "title": "(網頁版)取得具有簡單資訊的分店列表",
    "version": "0.1.0",
    "name": "Branch",
    "group": "Company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>語系 (zh 中文, en 英文)</p>"
          }
        ]
      }
    },
    "description": "<p>取得列表資訊。</p>",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "Company",
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"code\": \"hsinkun\",\n    \"name\": \"台中新光店\",\n    \"opening\": [\n            \"status\": \"true\",\n            \"text\": \"休息中\",\n   ], ...\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "POST",
    "url": "GBapi/Drink",
    "title": "取得飲品資訊",
    "version": "0.1.0",
    "name": "Drink",
    "group": "Company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>語系 (zh 中文, en 英文)</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "branch",
            "description": "<p>分店Code</p>"
          }
        ]
      }
    },
    "description": "<p>取得飲品資訊。</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "html",
            "description": "<p>items為空值時顯示Html參數。</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": \"drink\",\n  \"title\": \"特調飲品\",\n  \"desc\": \"\",\n  \"list\": [\n    {\n      \"id\": \"167\",\n      \"name\": \"鮮釀啤酒\",\n      \"code\": \"beer\",\n      \"img\": \"http://192.168.22.14/icms/uploads/items/7828e8b2-e246-445c-0c4f-ee9f8655bef5.jpg\",\n      \"items\": {\n        \"withImg\": [\n          {\n            \"id\": 216,\n            \"name\": \"小麥酵母愛爾\",\n            \"price\": {},\n            \"profile\": \"店內唯一的愛爾啤酒(Ale),帶著微微氣泡,有丁香 及香蕉等水果香味,口感清爽,別有一番特殊風味.\",\n            \"img\": \"http://192.168.22.14/icms/uploads/items/99e5d78c-3b7c-b1fa-c59e-2340ff88f5e7.jpg\"\n          },\n          {\n            \"id\": 217,\n            \"name\": \"史瓦茲黑啤酒\",\n            \"price\": {},\n            \"profile\": \"德語意為“黑啤酒”,但口味清爽甘醇,有如咖啡般 烘培的風味.曾獲得2006年世界啤酒盃及2007年美國 美國盛大啤酒節的金牌. 糖度:11.2°Plato , 苦度:21 , 酒精濃度:4.3%\",\n            \"img\": \"http://192.168.22.14/icms/uploads/items/49f35b45-2bf3-8ef2-067a-e2dfbf46a03a.jpg\"\n          }, ...\n        ],\n        \"noImg\": []\n      }\n    },\n    {\n      \"id\": \"168\",\n      \"name\": \"精選葡萄酒\",\n      \"code\": \"wine\",\n      \"img\": \"http://192.168.22.14/icms/uploads/items/d84904db-54b3-9a2f-2bbd-0e1676b185e3.jpg\",\n      \"items\": {\n        \"withImg\": [\n          {\n            \"id\": 222,\n            \"name\": \"卡本內蘇維農紅酒\",\n            \"price\": {},\n            \"profile\": \"卡本內蘇是世界最為流行的葡萄酒，起源於法國的波爾多地區，被稱為葡萄酒之王，您可以問問您的酒保，關於GB的嚴選紅酒的等級與其他紅酒的等級\\r\\n\",\n            \"img\": \"http://192.168.22.14/icms/uploads/items/67c3b41b-1268-961e-8d9b-312085e8d414.jpg\"\n          }, ...\n        ],\n        \"noImg\": []\n      }\n    }, ...\n    {\n      \"id\": \"170\",\n      \"name\": \"釀酒流程\",\n      \"code\": \"brewingpro\",\n      \"img\": \"http://192.168.22.14/icms/uploads/items/2256ae8a-543d-bced-8245-9952660f5fc5.jpg\",\n      \"items\": [],\n      \"html\": \"<div class=\\\"itemFullText\\\"><iframe width=\\\"560\\\" height=\\\"315\\\" src=\\\"http://www.youtube.com/embed/4pKO_ZXDOmo\\\" frameborder=\\\"0\\\"></iframe><table width=\\\"640\\\" align=\\\"center\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\"><tbody><tr><td align=\\\"left\\\" valign=\\\"top\\\"><p>&nbsp;</p><ol><li class=\\\"style9\\\">秤重與研磨：依循德國傳統的釀酒方式，將麥芽精確的秤重後，再經由磨麥機研磨。 </li><li class=\\\"style9\\\">將磨好的麥芽暫存於儲麥槽，再傳送至醣化桶進行下一個步驟。 </li><li class=\\\"style9\\\">醣化：將水加入麥芽中，即成為所謂的麥芽漿，然後開始進行一連串的升溫動作，將穀物內大分子澱粉分解成小分子的單醣。 </li><li class=\\\"style9\\\">濾出麥芽汁：將麥芽汁與麥芽外殼及雜質分離。 </li><li class=\\\"style9\\\">將濾出的麥芽汁與啤酒花煮沸。 </li><li class=\\\"style9\\\">將煮沸的麥芽汁冷卻。 </li><li class=\\\"style9\\\">初步發酵：將酵母加入冷卻後的麥芽汁內，酵母分解醣以後產生酒精與二氧化碳。 </li><li class=\\\"style9\\\">熟成：降低發酵桶的溫度，開始冷藏4~6周的時間，稱之為「啤酒熟成期」。 </li><li class=\\\"style9\\\">過濾：將啤酒內的酵母濾除。 </li><li class=\\\"style9\\\">供應：將啤酒移至供應桶內，再配送至餐廳。 </li></ol></td></tr></tbody></table></div>\"\n    },\n    {\n      \"id\": \"171\",\n      \"name\": \"GB鮮釀啤酒 得獎經歷\",\n      \"code\": \"awards\",\n      \"img\": \"http://192.168.22.14/icms/uploads/items/1edc0b1a-3637-8874-2dc4-9b5a898f8b2d.jpg\",\n      \"items\": [],\n      \"html\": \"<div class=\\\"itemFullText\\\"><table style=\\\"width: 500px;margin:0 auto;\\\" border=\\\"0\\\" cellspacing=\\\"2\\\" cellpadding=\\\"2\\\"><tbody><tr><td colspan=\\\"3\\\" align=\\\"center\\\"><h2 class=\\\"hdr1\\\">世界啤酒大賽<span class=\\\"field-content\\\">®</span></h2></td></tr><tr><td colspan=\\\"3\\\"><hr></td></tr><tr><td valign=\\\"top\\\"><table style=\\\"width: 160px;\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" align=\\\"center\\\"><tbody><tr><td align=\\\"center\\\"><br><div class=\\\"hdr2\\\">Gold Medal</div><img title=\\\"award-wbc-gold.gif\\\" alt=\\\"award-wbc-gold.gif\\\" src=\\\"http://www.gbarestaurants.com/images/award-wbc-gold.gif\\\">...</td></tr></tbody></table></td></tr></tbody></table></div>\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "Company"
  },
  {
    "type": "POST",
    "url": "GBapi/ItemsList",
    "title": "開卡頁面-取得地址/分店列表",
    "version": "0.1.0",
    "name": "ItemsList",
    "group": "Company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>語系 (zh 中文, en 英文)</p>"
          }
        ]
      }
    },
    "description": "<p>開卡頁面-取得地址/分店列表。</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "true",
            "description": "<p>取得成功。</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"cityList\": [\n         {   \"id\": \"23\", \"name\": \"新北市\" },\n         {   \"id\": \"22\", \"name\": \"台北市\" }, ...\n      ],\n    \"branchList\":  [\n         {   \"company_id\": \"1\", \"name\": \"台北信義店\", \"code\": \"sinyi\" },\n         {   \"company_id\": \"2\", \"name\": \"台北敦北店\", \"code\": \"dunhuan\" }, ...\n      ],\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "Company"
  },
  {
    "type": "POST",
    "url": "GBapi/Locations",
    "title": "取得分店資訊詳細內容",
    "version": "0.1.0",
    "name": "Locations",
    "group": "Company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>語系 (zh 中文, en 英文)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "lat",
            "description": "<p>使用者經度(for app)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "lng",
            "description": "<p>使用者緯度(for app)</p>"
          }
        ]
      }
    },
    "description": "<p>取得分店資訊詳細內容，輸入有效（經度緯度）會多回傳 distance單位 km</p> <p>預設排序為  信義/敦北/西北/林口/新光</p> <p>http://movable-type.co.uk/scripts/latlong.html 線上測試</p>",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "Company",
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n[{\n      \"name\": \"台北信義店\",\n      \"code\": \"sinyi\",\n      \"opening\": {\n        \"status\": ture,\n        \"text\": \"營業中\"\n      },\n      \"phone\": \"02-8786-7588\",\n      \"fax\": \"07-99999999\",\n      \"address\": \"台北市信義區松壽路11號2樓 (新光三越A11館2F)\",\n      \"geo\": {\n        \"lat\": \"25.036018\",\n        \"lng\": \"121.565041\"\n        \"distance\": \"299.45998512774\"\n      },\n      \"hours\": {\n        \"營業時間\": [11:00~24:00],\n        \"商業午餐\": [\"週一~週五:11:00~14:00\"],\n        \"Late Night\": [ \"週二~週日:21:30~23:00\"],\n        \"週末早午餐\": [],\n        \"Happy Hour\": [\"週一~週五:13:00~17:00\"]\n      },\n      \"img\": \"http://192.168.22.14/icms/uploads/companys/82215252-0fc2-57a2-48b2-dd28c637cd9c.jpg\",\n      \"streemap\": \"https://www.google.com/maps/embed?pb=!1m0!3m2!1szh-TW!2s!4v1478831746399!6m8!1m7!1s_71jKv7PB_cAAAQY-_tIzQ!2m2!1d25.05335818382042!2d121.5482644332556!3f284.3014508903652!4f-24.60248276781357!5f0.7820865974627469\",\n      \"desc\": \"本餐廳離捷運市政府站與台北101站步行只要五分鐘，\\r\\n\\r\\n附近還有新光三越新天地，\\r\\n\\r\\n可以讓您逛街，逛完餓了就可以來本餐廳享受美食，\\r\\n\\r\\n再來杯清涼的鮮釀啤酒，\\r\\n\\r\\n吃完還可以到旁邊信義威秀影城看最新的電影，\\r\\n\\r\\n或是到旁邊的世貿展參觀，\\r\\n\\r\\n每一期世貿展都會有最熱門最優惠的活動，\\r\\n\\r\\n讓您在一天的假日既充實又開心。\",\n      \"tags\":[{title: \"酥炸朝鮮薊心\", url: \"http://bandon.9ifriend.com/gb/zh/menu/appetizer/1/443?branch=sinyi\", weight: 1494924780078},{}]\n    }, {}..\n]",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "POST",
    "url": "GBapi/MainCarousel",
    "title": "取得首頁輪撥內容",
    "version": "0.1.0",
    "name": "MainCarousel",
    "group": "Company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>語系 (zh 中文, en 英文)</p>"
          }
        ]
      }
    },
    "description": "<p>取得首頁輪撥內容</p>",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "Company",
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n      \"marquees\": [\n        {\n          \"id\": \"11\",\n          \"date\": \"2017-01-20 15:55:50\",\n          \"desc\": \"GB目前正推出 聖誕新年套餐\"\n        }, ...\n      ],\n      \"carousels\": [\n        {\n          \"id\": \"87\",\n          \"url\": \"http://www.gbarestaurants.com.tw/\",\n          \"title\": \"季節限定 - 蘑菇香煎嫩雞\",\n          \"content\": \"將雞胸肉切成薄片後沾裹牛奶及香料麵粉，\\r\\n用奶油及橄欖油以半煎炸的方式煎出金黃的外衣，\\r\\n搭配以紅酒醬和梅爾森拉格為基底熬煮的蘑菇啤酒醬。\\r\\n我們透過刀工及料理方式改善雞胸肉的口感，\\r\\n鮮嫩的程度絕對顛覆你對雞胸肉乾柴的刻板印象！\\r\\n也利用這次的秋季菜單，\\r\\n讓原本就在我們主要菜單中的這道餐點煥然一\",\n         \"img\": \"http://210.203.20.28/icms/uploads/webs/adv/2329459d-1cdd-ed90-c2bd-be36dea77753.jpg\"\n        }, ...\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "POST",
    "url": "GBapi/Menu",
    "title": "（網頁版）取得菜單資訊",
    "version": "0.1.0",
    "name": "Menu",
    "group": "Company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>語系 (zh 中文, en 英文)</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "branch",
            "description": "<p>分店Code</p>"
          }
        ]
      }
    },
    "description": "<p>取得列表資訊。</p>",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "Company",
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n   code:\"menu\",\n   title:\"精緻美食\",\n   desc:\"探索我們更新選擇起動刺激，扣人心弦的主菜\"\n   list:[{\n         id: 1, \n         name: \"沙拉\", \n         code: \"salad\",\n         img: \"ccc.jpg\",\n         items: [{\n                 noImg: [{\n                 \t\tid:\"539\"\n                 \t\tname:\"主廚沙拉\"\n                 \t\tprice:{\"\": \"240\"}\n                 \t\tprofile:\"混合生菜加上蕃茄、黃瓜、培根、香料胡....\"\n                 },{\n                 \t\tid:\"540\",\n                 \t\tname:\"彩虹沙拉\",\n                 \t\tprice:{\"雞肉\": \"460\", \"海鮮\": \"590\"},\n                 \t\tprofile:\"混合生菜加上蕃茄、培根、水煮蛋和新鮮鱷梨\"\n                 }],\n                 withImg: [{\n         \t\t        id:\"535\",\n         \t        \tname:\"總匯羅蔓沙拉\",\n         \t        \tprice:{\"\": \"480\", \"加碳烤雞肉\": \"180\", \"加碳烤鮭魚\": \"120\"},\n         \t        \tprofile:\"混合生菜加上碳烤雞肉，搭配檸檬橄欖油醋醬\",\n         \t        \timg:\"89.jpg\"\t\n         \t\t}]\n         }]\n         }, {\n         id: 2, \n         \tname: \"開心點\", \n         \tcode: \"dessert\",\n         \t…….\n         }]\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "POST",
    "url": "GBapi/Nav",
    "title": "（網頁版）取得子頁面詳細資料",
    "version": "0.1.0",
    "name": "Nav",
    "group": "Company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>語系 (zh 中文, en 英文)</p>"
          },
          {
            "group": "Parameter",
            "type": "code",
            "optional": false,
            "field": "code",
            "description": "<p>頁面（aboutus, careers, news...）from SideMenu-&gt;code</p>"
          }
        ]
      }
    },
    "description": "<p>取得子頁面詳細資料。</p>",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "Company",
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    title: '',\n    code: 'careers',\n    desc: '',\n    nav: {\n    \ttype: [\n    \t\t{code:'careers', text:'人才招募'}, \n    \t\t{code:'culture', text:'文化'}, \n    \t\t{code:'training', text:'訓練'}, \n    \t\t{code:'benefit', text:'福利'}]\n    }       \n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "POST",
    "url": "GBapi/NearlyBranch",
    "title": "(網頁版)取得最短距離分店代號",
    "version": "0.1.0",
    "name": "NearlyBranch",
    "group": "Company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Float",
            "optional": false,
            "field": "lat",
            "description": "<p>經度</p>"
          },
          {
            "group": "Parameter",
            "type": "Float",
            "optional": false,
            "field": "lng",
            "description": "<p>緯度</p>"
          }
        ]
      }
    },
    "description": "<p>取得最短距離分店代號。</p>",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "Company",
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"code\": \"hsinkun\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "POST",
    "url": "GBapi/Page",
    "title": "取得頁面詳細資料",
    "version": "0.1.0",
    "name": "Page",
    "group": "Company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>語系 (zh 中文, en 英文)</p>"
          },
          {
            "group": "Parameter",
            "type": "code",
            "optional": false,
            "field": "code",
            "description": "<p>頁面code（aboutus, benefit, careers...）, 新增igb/support/, coupon, employee,gift</p> <p>相關代碼如下：</p> <ol> <li> <p>aboutus- 關於我們 （網頁版）</p> </li> <li> <p>privatedining- 派對包場服務 （網頁版）</p> </li> <li> <p>cinema- GB影片介紹 （網頁版）</p> </li> <li> <p>careers- 人才招募 （網頁版）</p> </li> <li> <p>culture- 文化 （網頁版）</p> </li> <li> <p>training- 訓練 （網頁版）</p> </li> <li> <p>benefit- 福利 （網頁版）</p> </li> <li> <p>support- iGB會員服務</p> </li> <li> <p>coupon- 優惠兌換</p> </li> <li> <p>employee- 優秀員工 （網頁版）</p> </li> <li> <p>gift- 開卡並獲得好禮</p> </li> <li> <p>getVoucher- 獲得優惠卷</p> </li> </ol>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "imagedata",
            "optional": false,
            "field": "main_pic",
            "description": "<p>側邊圖片(igb/support/, support, gift, coupun)</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "employee_title",
            "description": "<p>側邊圖片文字(coupun)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"code\": \"igb_member\",\n    \"html\": \"<div><div class=\\\"itemFullText\\\"><h1 style=\\\"margin-bottom:20px;\\\">GB鮮釀餐廳</div>\",\n    \"images\":[{\n        \"image_id\": \"123\",\n        \"type\": \"igb_member\",\n        \"company_id\": \"0\",\n        \"file_name\": \"d43f961c-809d-b963-a7ec-edaececf2993.jpg\",\n        \"link_name\": \"/tmp/phpGX4JHp\",\n        \"create_time\": \"2017-05-16 10:39:17\",\n        \"weight\": \"1494904040\",\n        \"url\": \"1\",\n        \"image_url\": \"https://test.dudooeat.com/icms/uploads/webs/adv/89d74bf2-9dd7-dbb6-21da-3506c6d1a8502.jpg\"\n        },{\n    }],\n    \"main_pic\":[{\n        \"image_id\": \"147\",\n        \"type\": \"support_main\",\n        \"company_id\": \"0\",\n        \"file_name\": \"f758776c-b3a5-5dba-3b20-694274e881aa.jp\",\n        \"link_name\": \"/tmp/phpGX4JHp\",\n        \"create_time\": \"2017-06-07 10:39:17\",\n        \"weight\": \"1494904040\",\n        \"url\": \"1\",\n        \"image_url\": \"https://test.dudooeat.com/icms/uploads/webs/adv/f758776c-b3a5-5dba-3b20-694274e881aa.jpg\"\n        }\n    }],\n    \"employee_title\": \"本月優秀店員\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>取得頁面資訊。</p>",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "Company"
  },
  {
    "type": "GET",
    "url": "GBapi/Reserve",
    "title": "取得線上訂位url",
    "version": "0.1.0",
    "name": "Reserve",
    "group": "Company",
    "description": "<p>取得線上訂位url</p>",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "Company",
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"http://tw.google.com\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "POST",
    "url": "GBapi/SearchLocations",
    "title": "門市查詢（for app）",
    "version": "0.1.0",
    "name": "SearchLocations",
    "group": "Company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>語系 (zh 中文, en 英文)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "searchString",
            "description": "<p>查詢名稱/字串</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "lat",
            "description": "<p>使用者經度</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "lng",
            "description": "<p>使用者緯度</p>"
          }
        ]
      }
    },
    "description": "<p>門市查詢，輸入有效（經度緯度）會多回傳 distance單位 km</p> <p>未給經緯度或缺一，就是預設排序，預設排序為  信義/敦北/西北/林口/新光。</p> <p>有距離是由近到遠排序。</p>",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "Company",
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n[{\n      \"name\": \"台北信義店\",\n      \"code\": \"sinyi\",\n      \"opening\": {\n        \"status\": ture,\n        \"text\": \"營業中\"\n      },\n      \"phone\": \"02-8786-7588\",\n      \"fax\": \"07-99999999\",\n      \"address\": \"台北市信義區松壽路11號2樓 (新光三越A11館2F)\",\n      \"geo\": {\n        \"lat\": \"25.036018\",\n        \"lng\": \"121.565041\"\n        \"distance\": \"299.45998512774\"\n      },\n      \"hours\": {\n        \"營業時間\": [11:00~24:00],\n        \"商業午餐\": [\"週一~週五:11:00~14:00\"],\n        \"Late Night\": [ \"週二~週日:21:30~23:00\"],\n        \"週末早午餐\": [],\n        \"Happy Hour\": [\"週一~週五:13:00~17:00\"]\n      },\n      \"img\": \"http://192.168.22.14/icms/uploads/companys/82215252-0fc2-57a2-48b2-dd28c637cd9c.jpg\",\n      \"streemap\": \"https://www.google.com/maps/embed?pb=!1m0!3m2!1szh-TW!2s!4v1478831746399!6m8!1m7!1s_71jKv7PB_cAAAQY-_tIzQ!2m2!1d25.05335818382042!2d121.5482644332556!3f284.3014508903652!4f-24.60248276781357!5f0.7820865974627469\",\n      \"desc\": \"本餐廳離捷運市政府站與台北101站步行只要五分鐘，\\r\\n\\r\\n附近還有新光三越新天地，\\r\\n\\r\\n可以讓您逛街，逛完餓了就可以來本餐廳享受美食，\\r\\n\\r\\n再來杯清涼的鮮釀啤酒，\\r\\n\\r\\n吃完還可以到旁邊信義威秀影城看最新的電影，\\r\\n\\r\\n或是到旁邊的世貿展參觀，\\r\\n\\r\\n每一期世貿展都會有最熱門最優惠的活動，\\r\\n\\r\\n讓您在一天的假日既充實又開心。\",\n      \"tags\":[{title: \"酥炸朝鮮薊心\", url: \"http://bandon.9ifriend.com/gb/zh/menu/appetizer/1/443?branch=sinyi\", weight: 1494924780078},{}]\n    }, {}..\n]",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "POST",
    "url": "GBapi/SideMenu",
    "title": "（網頁版）取得側邊menu資訊",
    "version": "0.1.0",
    "name": "SideMenu",
    "group": "Company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>語系 (zh 中文, en 英文)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "branch",
            "description": "<p>分店Code &quot;sinyi&quot;</p>"
          }
        ]
      }
    },
    "description": "<p>取得列表資訊。</p>",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "Company",
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"logo\": {\n         \"src\": \"\"\n    }\n    \"main\": [\n        {\n            \"name\": \"精緻美食\",\n            \"code\": \"menu\",\n            \"sub\": [\n                {\n                    \"code\": \"seafood\",\n                    \"id\": \"71\",\n                    \"img\": \"\",\n                    \"name\": \"海鮮\"\n                },\n                {\n                    \"code\": \"dessert\",\n                    \"id\": \"72\",\n                    \"img\": \"\",\n                    \"name\": \"開心點\"\n                }, ...\n             ]\n         },\n         {   \"name\": \"鮮釀啤酒\", \"code\": \"beer\" },\n         {   \"name\": \"最新消息\", \"code\": \"news\" },\n         {   \"name\": \"分店資訊\", \"code\": \"locations\" },\n         {   \"name\": \"線上訂位\", \"code\": \"order\" }\n   ],\n   \"second\": [\n         {   \"name\": \"關於我們\", \"code\": \"about\" },\n         {   \"name\": \"派對包場服務\", \"code\": \"party\" },\n         {   \"name\": \"人才招募\", \"code\": \"careers\" },\n         {   \"name\": \"聯絡我們\", \"code\": \"contact\" }\n   ],\n   \"third\": [\n         {  \"name\": \"iGB會員專區\", \"code\": \"iGB\" }\n   ]\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "POST",
    "url": "GBapi/getApplocales",
    "title": "取得文字語系設定（for app）",
    "version": "0.1.0",
    "name": "getApplocales",
    "group": "Company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>語系 (zh 中文, en 英文)</p>"
          }
        ]
      }
    },
    "description": "<p>文字語系設定。</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "keyword",
            "description": "<p>該文字項目唯一識別代號。</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>對應語系給文字顯示用。</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": [\n          {\n              \"Not_Login_Hello\": \"歡迎加入iGB會員<br />  您將享有更多專屬優惠\",\n              \"Login_Hello\": \"本月即將到期點數：[ExpiringPoint]<br />  永久點數：[PermanentBonus]<br />  儲值金餘額：[TTLDeposit]\",\n              \"Not_Enough_Point\": \"您再累積1,000點<br />  即可兌換指定商品<br />  請繼續加油！\",\n              \"Enough_Point\": \"恭喜您！<br />  已達兌換商品門檻\",\n              \"RedBtn_Consuming_Price\": \"消費紀錄\",\n              ...\n      ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "Company"
  },
  {
    "type": "POST",
    "url": "GBapi/getCompanyList",
    "title": "取得店家列表",
    "version": "0.1.0",
    "name": "getCompanyList",
    "group": "Company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>語系 (zh 中文, en 英文)</p>"
          }
        ]
      }
    },
    "description": "<p>取得最短距離分店代號。</p>",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "Company",
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": [\n        {\n            \"company_id\": \"1\",\n            \"name\": \"台北信義店\",\n            \"code\": \"sinyi\",\n            \"inline_code\": \"-KrUcxEi8rWRvWwZssL9\"\n        },     \n        {     \n            \"company_id\": \"2\",\n            \"name\": \"台北敦北店\",\n            \"code\": \"dunhuan\",\n            \"inline_code\": \"-KrUcxDEGdGov1kxPCwA\"\n        },\n        {\n            \"company_id\": \"5\",\n            \"name\": \"台北南西店\",\n            \"code\": \"nancy\",\n            \"inline_code\": \"-KrUdQFBddOxe7p6yiT5\"\n        },\n        {\n            \"company_id\": \"6\",\n            \"name\": \"台北林口店\",\n            \"code\": \"linco\",\n            \"inline_code\": \"-KrUcxDB3ZPhOGcwoFos\"\n        },\n        {\n            \"company_id\": \"4\",\n            \"name\": \"台中新光店\",\n            \"code\": \"hsinkun\",\n            \"inline_code\": \"-KrUcxD6seLOVaOxoWfo\"\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "POST",
    "url": "GBapi/Certification20",
    "title": "驗證信箱/手機 v2",
    "version": "0.1.0",
    "name": "Certification20",
    "group": "Member",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "account",
            "description": "<p>使用者信箱/電話</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>使用者姓名（寄信用）</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>驗證類型email/mobile</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "SMStoEmail",
            "description": "<p>測試用參數不傳簡訊，&quot;true&quot; 直接秀出簡訊內容。（網頁版）</p>"
          }
        ]
      }
    },
    "description": "<p>驗證信箱/手機</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "true",
            "description": "<p>發送成功。</p>"
          }
        ]
      }
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "Member"
  },
  {
    "type": "POST",
    "url": "GBapi_20/ChangePwd",
    "title": "token_變更密碼*",
    "version": "0.2.0",
    "name": "ChangePwd",
    "group": "Member",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登入token</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>使用者信箱('唯一'帳號)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "old_password",
            "description": "<p>舊使用者密碼(RSA encrypt)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "new_password",
            "description": "<p>新使用者密碼(RSA encrypt)</p>"
          }
        ]
      }
    },
    "description": "<p>註冊會員。</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "false",
            "description": "<p>密碼錯誤。</p>"
          }
        ]
      }
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi_20.php",
    "groupTitle": "Member"
  },
  {
    "type": "POST",
    "url": "GBapi/ChangePwd",
    "title": "token變更密碼",
    "version": "0.1.0",
    "name": "ChangePwd",
    "group": "Member",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>使用者信箱('唯一'帳號)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "old_password",
            "description": "<p>舊使用者密碼</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "new_password",
            "description": "<p>新使用者密碼</p>"
          }
        ]
      }
    },
    "description": "<p>註冊會員。</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "false",
            "description": "<p>密碼錯誤。</p>"
          }
        ]
      }
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "Member"
  },
  {
    "type": "POST",
    "url": "GBapi_20/ConfirmedMobile",
    "title": "確認email/mobile驗證碼",
    "version": "0.2.0",
    "name": "ConfirmedMobile",
    "group": "Member",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "account",
            "description": "<p>信箱 or 手機</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>驗證碼</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>驗證類型email/mobile</p>"
          }
        ]
      }
    },
    "description": "<p>確認email/mobile驗證碼。 10分鐘</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "true",
            "description": "<p>認證成功。</p>"
          }
        ]
      }
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi_20.php",
    "groupTitle": "Member"
  },
  {
    "type": "POST",
    "url": "GBapi/ConfirmedMobile",
    "title": "確認mobile驗證",
    "version": "0.1.0",
    "name": "ConfirmedMobile",
    "group": "Member",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>手機</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>手機驗證碼</p>"
          }
        ]
      }
    },
    "description": "<p>確認mobile驗證。 10分鐘</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "true",
            "description": "<p>認證成功。</p>"
          }
        ]
      }
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "Member"
  },
  {
    "type": "POST",
    "url": "GBapi/ContactForm",
    "title": "取得客服中心頁面",
    "version": "0.1.0",
    "name": "ContactForm",
    "group": "Member",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>語系 (zh 中文, en 英文)</p>"
          }
        ]
      }
    },
    "description": "<p>取得客服中心頁面</p>",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "Member",
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n   \"way\": [\n      { \"contact_item_id\": \"-1\",  \"name\": \"請選擇聯絡方式\" },\n      { \"contact_item_id\": \"1\", \"name\": \"電話\" },\n      { \"contact_item_id\": \"2\", \"name\": \"電子郵件\" },\n      { \"contact_item_id\": \"3\", \"name\": \"簡訊\" }\n    ],\n    \"type\": [\n      { \"contact_item_id\": \"-1\", \"name\": \"請選擇問題種類\" },\n      { \"contact_item_id\": \"4\", \"name\": \"餐廳體驗\" },\n      { \"contact_item_id\": \"5\", \"name\": \"GB禮卷\" },\n      {  \"contact_item_id\": \"6\", \"name\": \"網站問題\"  },\n      {  \"contact_item_id\": \"7\", \"name\": \"iGB問題\" }\n    ],\n    \"comments\": \"請輸入您想詢問的問題或您對產品與服務的需求\",\n    \"sideTitle\": {\n      \"title\": \"客服中心\",\n      \"name\": \"姓名\",\n      \"phone\": \"手機號碼\",\n      \"email\": \"電子郵件\",\n      \"way\": \"希望聯絡方式\",\n      \"type\": \"問題種類\",       \n      \"comments\": \"問題或建議內容\"\n    }\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "POST",
    "url": "GBapi/ContactRequest",
    "title": "寄送客服問題",
    "version": "0.1.0",
    "name": "ContactRequest",
    "group": "Member",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>名字 (必填)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>電話 (必填)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>信箱 (必填)</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "way",
            "description": "<p>contact_item_id</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "type",
            "description": "<p>contact_item_id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "comments",
            "description": "<p>問題或建議內容</p>"
          }
        ]
      }
    },
    "description": "<p>寄送客服問題。</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "true",
            "description": "<p>寄送成功。</p>"
          }
        ]
      }
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "Member"
  },
  {
    "type": "POST",
    "url": "GBapi_20/Login",
    "title": "token_登錄*",
    "version": "0.2.0",
    "name": "Login",
    "group": "Member",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "account",
            "description": "<p>使用者信箱/電話</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>使用者密碼(RSA encrypt)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "device_token",
            "description": "<p>裝置Token (for app)，須帶[platform]參數</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "platform",
            "description": "<p>裝置類型 ios, android (for app)</p>"
          }
        ]
      }
    },
    "description": "<p>取得註冊會員與表格資訊。</p> <ol> <li>testIP外網: http://54.64.115.3/</li> <li>testEmail: littleyeh0111@gmail.com</li> <li>testPW: bb7e35</li> </ol>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "point_threshold",
            "description": "<p>點數兌換門檻</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "notification",
            "description": "<p>通知開關 （0 關 / 1 開）</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": \"true\",\n    \"name\": \"Philip Chan\",\n    \"email\": \"littleyeh0111@gmail.com\",\n    \"mobile\": \"09913622xx\",\n    \"token\": \"B34E72FECAF7303257714DA9E7AFA792\",\n    \"card_member\": \"00808501210231321\",\n    \"notification:\"1\",\n    \"point_threshold\": \"3000\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "AuthEmployeeLoginError",
            "description": "<p>login_invalid_username_password</p>"
          }
        ]
      }
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi_20.php",
    "groupTitle": "Member"
  },
  {
    "type": "POST",
    "url": "GBapi/Login",
    "title": "token登錄",
    "version": "0.1.0",
    "name": "Login",
    "group": "Member",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>語系 (zh 中文, en 英文)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "account",
            "description": "<p>使用者信箱/電話</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>使用者密碼</p>"
          }
        ]
      }
    },
    "description": "<p>取得註冊會員與表格資訊。</p> <ol> <li>testIP外網: http://210.203.20.28/</li> <li>testEmail: littleyeh0111@gmail.com</li> <li>testPW: bb7e35</li> </ol>",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "AuthEmployeeLoginError",
            "description": "<p>login_invalid_username_password</p>"
          }
        ]
      }
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "Member",
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": \"true\",\n    \"member_id\": \"54967\",\n    \"card_member\": \"0820000000100941\",\n    \"name\": \"Philip Chan\",\n    \"point\": \"-\",\n    \"balance\": \"-\",\n    \"coupons\":[\n         {\n             \"num\": \"2529014\",\n             \"name\": \"Anniversary\",\n             \"text\": \"NTD 300 coupon for anniversary\",\n             \"expired\": \"2017-04-15 11:18:56.0\",\n         }, ...\n    ],\n    \"used_coupons\": [\n         {\n             \"no\": \"2529048\",\n             \"name\": \"New Signup\",\n             \"text\": \"Sign up award\",\n             \"expired\": \"2017-01-15 11:18:56.0\",\n         },{\n             \"no\": \"2529022\",\n             \"name\": \"Birthday\",\n             \"text\": \"Birthday Coupon\",\n             \"expired\": \"2017-01-15 11:18:56.0\",\n         }, ...\n    ],\n    \"transations\": [\n         {\n             \"no\": \"912\",\n             \"date\": \"2013-01-15 11:18:56.0\",\n             \"store\": \"ShinKong Store\",\n             \"money\": \"0\",\n             \"before_point\": \"0.00\",\n             \"add_point\": \"\",\n             \"point\": \"0.00\"\n         },{\n             \"no\": \"988\",\n             \"date\": \"2013-02-18 16:38:47.0\",\n             \"store\": \"ShinKong Store\",\n             \"money\": \"484\",\n             \"before_point\": \"0.00\",\n             \"add_point\": \"4.00\",\n             \"point\": \"4.00\"\n         }, ...\n    ],\n    \"sideTitle\": {\n             \"Title\": \"您好\",\n             \"topTitle\": \"帳戶總覽\",\n             \"topCardID\": \"iGB卡號\",\n             \"topPoint\": \"累積紅利點數\",\n             \"topRewards\": \"累積回饋金\",\n             \"Ecoupon\": \"優惠卷\",\n             \"SN\": \"序號\",\n             \"name\": \"名稱\",\n             \"Description\": \"說明\",\n             \"ExpiredDate\": \"截止日\",\n             \"UsedCoupons\": \"已使用優惠卷\",\n             \"Transaction\": \"交易明細\",\n             \"BranchStore\": \"分店\",\n             \"Consuming\": \"消費金額\",\n             \"preConsuming\": \"消費前點數\",\n             \"TheNew\": \"本次新增\",\n             \"Accumulate\": \"累積點數\"\n    }\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "POST",
    "url": "GBapi_20/Logout",
    "title": "token_登出",
    "version": "0.1.0",
    "name": "Logout",
    "group": "Member",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登入token</p>"
          }
        ]
      }
    },
    "description": "<p>登出。</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "true",
            "description": "<p>更新成功。</p>"
          }
        ]
      }
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi_20.php",
    "groupTitle": "Member"
  },
  {
    "type": "POST",
    "url": "GBapi_20/MemberMaintain",
    "title": "token_取得註冊會員維護頁面",
    "version": "0.2.0",
    "name": "MemberMaintain",
    "group": "Member",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>語系</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登入token</p>"
          }
        ]
      }
    },
    "description": "<p>取得註冊會員與表格資訊。</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "gender_id",
            "description": "<p>男(1) 、女(2)。</p>"
          },
          {
            "group": "Success 200",
            "type": "tinyint",
            "optional": false,
            "field": "is_subscribed",
            "description": "<p>訂閱(-1) 、不訂閱(0)。</p>"
          },
          {
            "group": "Success 200",
            "type": "tinyint",
            "optional": false,
            "field": "flag_email_validated",
            "description": "<p>已驗證(-1) 、未驗證(0)。</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varification",
            "description": "<p>為空值，表示使用者沒有認證信箱。</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"memberInfo\": {\n             \"member_id\": \"75\",\n             \"card_member\": \"0820000000100065\",\n             \"language_id\": \"2\",\n             \"name\": \"Cindy Chang\",\n             \"gender_id\": \"2\",\n             \"city_id\": \"22\",\n             \"address\": \"信義區\",\n             \"mobile\": \"0985873696\",\n             \"email\": \"yiwish@hotmail.com\",\n             \"flag_email_validated\": \"-1\",\n             \"branch_store_id\": \"2\",\n             \"is_subscribed\": \"0\",\n             \"birthday\": \"1990-10-27\"\n    },\n    \"cityList\": [\n         {   \"id\": \"23\", \"name\": \"新北市\" },\n         {   \"id\": \"22\", \"name\": \"台北市\" }, ...\n      ],\n    \"branchList\":  [\n         {   \"company_id\": \"1\", \"name\": \"台北信義店\", \"code\": \"sinyi\" },\n         {   \"company_id\": \"2\", \"name\": \"台北敦北店\", \"code\": \"dunhuan\" }, ...\n      ],\n    \"langlist\": [\n         {   \"lang\": \"en\", \"content\": \"English\", \"language_id\": \"1\" },\n         {   \"lang\": \"zh\", \"content\": \"繁體中文\", \"language_id\": \"2\" }\n      ],\n    \"sideTitle\": {\n             \"cardNumber\": \"iGB卡號\",\n             \"name\": \"姓名\",\n             \"phone\": \"手機\",\n             \"birthday\": \"生日\",\n             \"gender\": \"性別\",\n             \"email\": \"信箱\",\n             \"password\": \"密碼\",\n             \"lang\": \"語言\",\n             \"address\": \"聯絡地址\",\n             \"purchaseBranch\": \"購買分店\",\n             \"subscription\": \"訂閱電子報\",\n             \"memorialday\": \"紀念日\",\n             \"tip\": \"備註\",\n             \"board\": \"若需要修改信箱、電話、生日或紀念日，請聯絡<a href=\\\"HTTP_URL\\\" style=\\\"color:red;\\\">客服中心</a>處理\",\n             \"telephone\": \"聯絡電話\",\n             \"varification\": \"\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "GetUserError",
            "description": ""
          }
        ]
      }
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi_20.php",
    "groupTitle": "Member"
  },
  {
    "type": "POST",
    "url": "GBapi/MemberMaintain",
    "title": "token取得註冊會員維護頁面",
    "version": "0.1.0",
    "name": "MemberMaintain",
    "group": "Member",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>語系 (zh 中文, en 英文)</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "customer_id",
            "description": "<p>使用者ID</p>"
          }
        ]
      }
    },
    "description": "<p>取得註冊會員與表格資訊。</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "gender_id",
            "description": "<p>男(1) 、女(2)。</p>"
          },
          {
            "group": "Success 200",
            "type": "tinyint",
            "optional": false,
            "field": "is_subscribed",
            "description": "<p>訂閱(-1) 、不訂閱(0)。</p>"
          },
          {
            "group": "Success 200",
            "type": "tinyint",
            "optional": false,
            "field": "flag_email_validated",
            "description": "<p>已驗證(-1) 、未驗證(0)。</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varification",
            "description": "<p>為空值，表示使用者沒有認證信箱。</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"memberInfo\": {\n             \"member_id\": \"75\",\n             \"card_member\": \"0820000000100065\",\n             \"language_id\": \"2\",\n             \"name\": \"Cindy Chang\",\n             \"gender_id\": \"2\",\n             \"city_id\": \"22\",\n             \"address\": \"信義區\",\n             \"mobile\": \"0985873696\",\n             \"email\": \"yiwish@hotmail.com\",\n             \"flag_email_validated\": \"-1\",\n             \"branch_store_id\": \"2\",\n             \"is_subscribed\": \"0\",\n             \"birthday\": \"1990-10-27\"\n    },\n    \"cityList\": [\n         {   \"id\": \"23\", \"name\": \"新北市\" },\n         {   \"id\": \"22\", \"name\": \"台北市\" }, ...\n      ],\n    \"branchList\":  [\n         {   \"company_id\": \"1\", \"name\": \"台北信義店\", \"code\": \"sinyi\" },\n         {   \"company_id\": \"2\", \"name\": \"台北敦北店\", \"code\": \"dunhuan\" }, ...\n      ],\n    \"langlist\": [\n         {   \"lang\": \"en\", \"content\": \"English\", \"language_id\": \"1\" },\n         {   \"lang\": \"zh\", \"content\": \"繁體中文\", \"language_id\": \"2\" }\n      ],\n    \"sideTitle\": {\n             \"cardNumber\": \"iGB卡號\",\n             \"name\": \"姓名\",\n             \"phone\": \"手機\",\n             \"birthday\": \"生日\",\n             \"gender\": \"性別\",\n             \"email\": \"信箱\",\n             \"password\": \"密碼\",\n             \"lang\": \"語言\",\n             \"address\": \"聯絡地址\",\n             \"purchaseBranch\": \"購買分店\",\n             \"subscription\": \"訂閱電子報\",\n             \"memorialday\": \"紀念日\",\n             \"tip\": \"備註\",\n             \"board\": \"若需要修改信箱、電話、生日或紀念日，請聯絡<a href=\\\"HTTP_URL\\\" style=\\\"color:red;\\\">客服中心</a>處理\",\n             \"telephone\": \"聯絡電話\",\n             \"varification\": \"\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "GetUserError",
            "description": "<p>error customer id</p>"
          }
        ]
      }
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "Member"
  },
  {
    "type": "POST",
    "url": "GBapi_20/MemberUpdate",
    "title": "token_更新會員資料",
    "version": "0.2.0",
    "name": "MemberUpdate",
    "group": "Member",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登入token</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>名字</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "gender_id",
            "description": "<p>性別</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "language_id",
            "description": "<p>語系</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "city_id",
            "description": "<p>城市</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>地址</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telephone",
            "description": "<p>電話</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "branch_store_id",
            "description": "<p>分店分店ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Bool",
            "optional": false,
            "field": "is_subscribed",
            "description": "<p>是否訂閱</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "morialday",
            "description": "<p>紀念日</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tip",
            "description": "<p>備註</p>"
          }
        ]
      }
    },
    "description": "<p>更新會員資料。</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "true",
            "description": "<p>更新成功。</p>"
          }
        ]
      }
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi_20.php",
    "groupTitle": "Member"
  },
  {
    "type": "POST",
    "url": "GBapi/MemberUpdate",
    "title": "token更新會員資料",
    "version": "0.1.0",
    "name": "MemberUpdate",
    "group": "Member",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "member_id",
            "description": "<p>會員ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>名字</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "gender_id",
            "description": "<p>性別</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "language_id",
            "description": "<p>語系</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "city_id",
            "description": "<p>城市</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>地址</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "branch_store_id",
            "description": "<p>分店分店ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Bool",
            "optional": false,
            "field": "is_subscribed",
            "description": "<p>是否訂閱</p>"
          }
        ]
      }
    },
    "description": "<p>更新會員資料。</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "true",
            "description": "<p>更新成功。</p>"
          }
        ]
      }
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "Member"
  },
  {
    "type": "POST",
    "url": "GBapi/Record",
    "title": "取得帳單補登表單",
    "version": "0.1.0",
    "name": "Record",
    "group": "Member",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>語系 (zh 中文, en 英文)</p>"
          }
        ]
      }
    },
    "description": "<p>取得帳單補登表單</p>",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "Member",
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"branchList\":  [\n         {   \"company_id\": \"1\", \"name\": \"台北信義店\", \"code\": \"sinyi\" },\n         {   \"company_id\": \"2\", \"name\": \"台北敦北店\", \"code\": \"dunhuan\" }, ...\n      ],\n    \"sideTitle\": {\n      \"title\": \"帳單補登\",\n      \"name\": \"姓名\",\n      \"phone\": \"手機\",\n      \"email\": \"信箱\",\n      \"recordNumber\": \"帳單號碼\",\n      \"consumptionDate\": \"消費日期\",\n      \"consumptionMoney\": \"消費金額\",\n      \"consumptionBranch\": \"消費門市\"\n    }\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "POST",
    "url": "GBapi_20/RecordUpdate",
    "title": "token_送出帳單補登",
    "version": "0.2.0",
    "name": "RecordUpdate",
    "group": "Member",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登入token</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cardID",
            "description": "<p>卡號 (必填)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>名字 (必填)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>電話 (必填)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>信箱 (必填)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "recordNumber",
            "description": "<p>帳單號碼(必填)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "consumptionDate",
            "description": "<p>消費日期yyyy-mm-dd (必填)</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "consumptionMoney",
            "description": "<p>消費金額(必填)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "consumptionBranch",
            "description": "<p>消費門市&quot;sinyi&quot; (必填)</p>"
          }
        ]
      }
    },
    "description": "<p>送出帳單補登</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "true",
            "description": "<p>寄送成功。</p>"
          }
        ]
      }
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi_20.php",
    "groupTitle": "Member"
  },
  {
    "type": "POST",
    "url": "GBapi/RecordUpdate",
    "title": "token送出帳單補登(缺API)",
    "version": "0.1.0",
    "name": "RecordUpdate",
    "group": "Member",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cardID",
            "description": "<p>卡號 (必填)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>名字 (必填)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>電話 (必填)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>信箱 (必填)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "recordNumber",
            "description": "<p>帳單號碼(必填)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "consumptionDate",
            "description": "<p>消費日期yyyy-mm-dd (必填)</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "consumptionMoney",
            "description": "<p>消費金額(必填)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "consumptionBranch",
            "description": "<p>消費門市&quot;sinyi&quot; (必填)</p>"
          }
        ]
      }
    },
    "description": "<p>送出帳單補登</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "true",
            "description": "<p>寄送成功。</p>"
          }
        ]
      }
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "Member"
  },
  {
    "type": "POST",
    "url": "GBapi/UniqueDuplicate",
    "title": "查詢信箱/手機是否重複",
    "version": "0.1.0",
    "name": "UniqueDuplicate",
    "group": "Member",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>驗證種類 email / mobile</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "unique",
            "description": "<p>使用者信箱 / 手機('唯一'帳號)</p>"
          }
        ]
      }
    },
    "description": "<p>查詢信箱/手機是否重複。</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "true",
            "description": "<p>表示重複。</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "200 Request",
          "content": "HTTP/1.1 200 ok\n{\n  true\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Error",
            "description": "<p>The input value of [type] is unacceptable.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "400 Response",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"code\": 11,\n  \"error\": \"The input value of [type] is unacceptable.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "Member"
  },
  {
    "type": "POST",
    "url": "GBapi_20/forgetPassword",
    "title": "忘記密碼，取得暫時登入碼*",
    "version": "0.2.0",
    "name": "forgetPassword",
    "group": "Member",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>使用者信箱</p>"
          }
        ]
      }
    },
    "description": "<p>忘記密碼，取得暫時登入碼。</p> <ol> <li>長度10的亂數密碼（大小寫英文+數字）</li> </ol>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "true",
            "description": "<p>重置密碼成功。</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "200 Request",
          "content": "HTTP/1.1 200 ok\n{\n  true\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi_20.php",
    "groupTitle": "Member"
  },
  {
    "type": "POST",
    "url": "GBapi/forgetPassword",
    "title": "忘記密碼，取得暫時登入碼",
    "version": "0.1.0",
    "name": "forgetPassword",
    "group": "Member",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>使用者信箱</p>"
          }
        ]
      }
    },
    "description": "<p>忘記密碼，取得暫時登入碼。</p> <ol> <li>長度10的亂數密碼（大小寫英文+數字）</li> </ol>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "true",
            "description": "<p>重置密碼成功。</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "200 Request",
          "content": "HTTP/1.1 200 ok\n{\n  true\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "Member"
  },
  {
    "type": "POST",
    "url": "news/basic",
    "title": "1.Basic",
    "version": "0.1.0",
    "name": "basic",
    "group": "News",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>語系</p>"
          }
        ]
      }
    },
    "description": "<p>取得基本資訊。</p>",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/News.php",
    "groupTitle": "News",
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"title\": \"最新消息\",\n    \"code\": \"news\",\n    \"desc\": \"探索我們更新選擇起動刺激，扣人心弦的主菜\",\n    \"nav\": {\n        \"type\": [\n            {\n                \"code\": \"all\",\n                \"text\": \"所有消息\"\n            },\n            {\n                \"code\": \"offers\",\n                \"text\": \"最新優惠\"\n            },\n            {\n                \"code\": \"announcement\",\n                \"text\": \"最新公告\"\n            }\n        ]\n    }\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "POST",
    "url": "GBapi/getNewDetail",
    "title": "取得單一消息完整內容",
    "version": "0.1.0",
    "name": "getNewDetail",
    "group": "News",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>語系 (zh 中文, en 英文)</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "id",
            "description": "<p>消息id</p>"
          }
        ]
      }
    },
    "description": "<p>取得單一消息完整內容。</p>",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "News",
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": \"12\",\n  \"type\": {\n       \"code\": \"announcement\",\n       \"text\": \"最新公告\"\n  },\n  \"date\": \"2017.02.22\",\n  \"title\": \"Festbier季節套餐\",\n  \"brief\": \"<p>在每一個節日來臨時，您總是煩惱要送什麼東西給您最特別的朋友<br/>\\n<br/>\\n讓他們可以收到最美味的禮物<br/>\\nGB鮮釀餐廳禮券適合任何慶祝活動，生日!婚禮!員工獎勵!<br/>\\n請他們來到GB享受美食與啤酒。<br/>\\n您可以選擇您需要購買的金額，與您需求的面額。<br/>\\n<br/>\\n<br/>\\n如欲購買請洽各店經理<br/>\\n或撥打02-2912-7588分機105由GB客服人員為您服務</p>\",\n  \"img\": \"http://lacucinainglese.com/wp-content/uploads/2013/05/Scones.jpg\" *,\n  \"prev\": \n    {\n      \"id\": \"11\",\n      \"type\": {\n        \"code\": \"offers\",\n        \"text\": \"最新優惠\"\n      },\n      \"date\": \"2017.01.20\",\n      \"title\": \"GB目前正推出 聖誕新年套餐\"\n  },\n  \"next\": \n    {\n      \"id\": \"13\",\n      \"type\": {\n        \"code\": \"announcement\",\n        \"text\": \"最新公告\"\n      },\n      \"date\": \"2017.02.22\",\n      \"title\": \"HAPPY MOTHERS DAY 陪伴媽媽吃頓飯\"\n  }\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "POST",
    "url": "news/listAll",
    "title": "3.listAll",
    "version": "0.1.0",
    "name": "list",
    "group": "News",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>語系</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "kind",
            "description": "<p>訊息類別 （all 全、offers 最新優惠、announcement 最新公告）</p>"
          }
        ]
      }
    },
    "description": "<p>取得所有列表資訊。</p>",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/News.php",
    "groupTitle": "News",
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"page\": {\n        \"total\": \"6\",  // 前端網頁，總共有幾筆\n        \"unit\": 6 // 前端網頁，一頁顯示筆數，limit\n    },\n    \"query\": {\n        \"page\": \"1\", // 前端網頁，目前第幾頁\n        \"kind\": \"所有消息\"\n    },\n    \"list\": [\n        {\n            \"id\": \"1\",\n            \"type\": {\n                \"code\": \"offers\",\n                \"text\": \"最新優惠\"\n            },\n            \"date\": \"2016.12.01\",\n            \"title\": \"最新消息2\",\n            \"brief\": \"最新消息\",\n            \"img\": \"http://192.168.22.14/icms/images/noImg.png\"\n        },...\n        \n    ]\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "POST",
    "url": "news/list",
    "title": "2.list",
    "version": "0.1.0",
    "name": "list",
    "group": "News",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>語系</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>分頁</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "limit",
            "description": "<p>分頁筆數，預設是6筆</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "kind",
            "description": "<p>訊息類別 （all 全、offers 最新優惠、announcement 最新公告）</p>"
          }
        ]
      }
    },
    "description": "<p>取得列表資訊。</p>",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/News.php",
    "groupTitle": "News",
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"page\": {\n        \"total\": \"6\",  // 前端網頁，總共有幾筆\n        \"unit\": 6 // 前端網頁，一頁顯示筆數，limit\n    },\n    \"query\": {\n        \"page\": \"1\", // 前端網頁，目前第幾頁\n        \"kind\": \"所有消息\"\n    },\n    \"list\": [\n        {\n            \"id\": \"1\",\n            \"type\": {\n                \"code\": \"offers\",\n                \"text\": \"最新優惠\"\n            },\n            \"date\": \"2016.12.01\",\n            \"title\": \"最新消息2\",\n            \"brief\": \"最新消息\",\n            \"img\": \"http://192.168.22.14/icms/images/noImg.png\"\n        },...\n        \n    ]\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "POST",
    "url": "GBapi_20/getNewsNotReadCount",
    "title": "token_取得新聞通知未讀數量（for app）",
    "version": "0.1.0",
    "name": "getNewsNotReadCount",
    "group": "Notification",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登入token</p>"
          }
        ]
      }
    },
    "description": "<p>取得通知未讀數量，取得一個月內的未讀通知數量。</p>",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi_20.php",
    "groupTitle": "Notification",
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": 2\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "POST",
    "url": "GBapi_20/getNewsNotReadList",
    "title": "token_取得新聞通知列表（for app）",
    "version": "0.1.0",
    "name": "getNewsNotReadList",
    "group": "Notification",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登入token</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>語系 (zh 中文, en 英文)</p>"
          }
        ]
      }
    },
    "description": "<p>取得通知列表，取得一個月內的通知紀錄。</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "isRead",
            "description": "<p>0: 未讀、1:已讀</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": [\n        {\n            \"title\": \"iGB 回饋升級，點點都成金\",\n            \"notification_id\": \"1\",\n            \"post_time\": \"2018-05-11 08:34:49\"\n        },\n        {\n            \"title\": \"禮物卡上市\",\n            \"title\": \"2\",\n            \"post_time\": \"2018-05-01 11:34:49\"\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi_20.php",
    "groupTitle": "Notification"
  },
  {
    "type": "POST",
    "url": "GBapi_20/getNotReadCount",
    "title": "token_取得通知未讀數量（for app）",
    "version": "0.1.0",
    "name": "getNotReadCount",
    "group": "Notification",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登入token</p>"
          }
        ]
      }
    },
    "description": "<p>取得通知未讀數量，取得一個月內的未讀通知數量。</p>",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi_20.php",
    "groupTitle": "Notification",
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": 2\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "POST",
    "url": "GBapi_20/getNotReadList",
    "title": "token_取得通知列表（for app）",
    "version": "0.1.0",
    "name": "getNotReadList",
    "group": "Notification",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登入token</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>語系 (zh 中文, en 英文)</p>"
          }
        ]
      }
    },
    "description": "<p>取得通知列表，取得一個月內的通知紀錄。</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "isRead",
            "description": "<p>0: 未讀、1:已讀</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": [\n        {\n            \"title\": \"iGB 回饋升級，點點都成金\",\n            \"notification_id\": \"1\",\n            \"post_time\": \"2018-05-11 08:34:49\"\n        },\n        {\n            \"title\": \"禮物卡上市\",\n            \"title\": \"2\",\n            \"post_time\": \"2018-05-01 11:34:49\"\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi_20.php",
    "groupTitle": "Notification"
  },
  {
    "type": "POST",
    "url": "GBapi_20/setIsRead",
    "title": "token_通知更新已讀狀態（for app）",
    "version": "0.1.0",
    "name": "setIsRead",
    "group": "Notification",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登入token</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "notification_id",
            "description": "<p>通知id</p>"
          }
        ]
      }
    },
    "description": "<p>通知更新已讀狀態。</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "true",
            "description": "<p>更新成功。</p>"
          }
        ]
      }
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi_20.php",
    "groupTitle": "Notification"
  },
  {
    "type": "POST",
    "url": "GBapi_20/setNotificationSwitch",
    "title": "token_開啟關閉通知（app）",
    "version": "0.2.0",
    "name": "setNotificationSwitch",
    "group": "Notification",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登入token</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "notification",
            "description": "<p>通知關 0 / 開 1</p>"
          }
        ]
      }
    },
    "description": "<p>開啟關閉通知。</p>",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi_20.php",
    "groupTitle": "Notification",
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": true,\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "AuthTokenError Response",
          "content": "HTTP/1.1 200 Bad Request\n{\n    \"success\": false,\n    \"error\": \"Token error or expired\"\n}",
          "type": "json"
        },
        {
          "title": "APIError Response",
          "content": "HTTP/1.1 200 Bad Request\n{\n    \"success\": false,\n    \"error\": \"input error, please check again\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "POST",
    "url": "GBapi/CancelEDM",
    "title": "取消訂閱EDM(不用接)",
    "version": "0.1.0",
    "name": "CancelEDM",
    "group": "Server",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "t",
            "description": "<p>使用者信箱</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tid",
            "description": "<p>EDM id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cid",
            "description": "<p>user id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "ec",
            "description": "<p>event type</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "ea",
            "description": "<p>happend</p>"
          }
        ]
      }
    },
    "description": "<p>取消訂閱EDM。 for gmail機制</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "true",
            "description": "<p>取消成功。</p>"
          }
        ]
      }
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "Server"
  },
  {
    "type": "POST",
    "url": "GBapi/Confirmed",
    "title": "確認Email驗證(不用接)",
    "version": "0.1.0",
    "name": "Confirmed",
    "group": "Server",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tNo",
            "description": "<p>Hash</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "account",
            "description": "<p>使用者姓名/手機</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "etype",
            "description": "<p>驗證類型email(1)/mobile(2)</p>"
          }
        ]
      }
    },
    "description": "<p>確認Email驗證(server)。 10分鐘</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "true",
            "description": "<p>認證成功。</p>"
          }
        ]
      }
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "Server"
  },
  {
    "type": "POST",
    "url": "GBapi_20/CheckAccount",
    "title": "token_取得Account資訊 (不用接)",
    "version": "0.2.0",
    "name": "CheckAccount",
    "group": "Third_party",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>語系</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登入token</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "transations",
            "description": "<p>交易紀錄 (飛訊: WBWM)</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "qryBonusEx",
            "description": "<p>點數 (飛訊: WBWM)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": \"true\",\n    \"member_id\": \"70154\",\n    \"card_member\": \"0820000000446993\",\n    \"name\": \"GB test\",\n    \"qryBonusEx\": {\n         \"PermanentBonus\": \"2096\",\n         \"ExpiringPoint\": \"10000\",\n          \"QryBonusExList\": [\n            {\n              \"ExMonth\": \"2017-05\",\n              \"ExBonus\": \"10000\"\n            }\n          ]\n        },\n     \"transations\": [\n          {\n            \"no\": 0,\n            \"date\": \"5/31/2017 2:31:18 PM\",\n            \"store\": \"TAIPEI HSINYI STORE\",\n            \"money\": \"2000\",\n            \"before_point\": \"\",\n            \"add_point\": \"0\",\n            \"point\": \"789\"\n          },\n          {\n            \"no\": 1,\n            \"date\": \"5/31/2017 2:31:18 PM\",\n            \"store\": \"TAIPEI HSINYI STORE\",\n            \"money\": \"3000\",\n            \"before_point\": \"\",\n            \"add_point\": \"0\",\n            \"point\": \"789\"\n          }, ...]\n    \"sideTitle\": {\n             \"Title\": \"您好\",\n             \"topTitle\": \"帳戶總覽\",\n             \"topCardID\": \"iGB卡號\",\n             \"topPoint\": \"累積紅利點數\",\n             \"topRewards\": \"累積回饋金\",\n             \"Ecoupon\": \"優惠卷\",\n             \"SN\": \"序號\",\n             \"name\": \"名稱\",\n             \"Description\": \"說明\",\n             \"ExpiredDate\": \"截止日\",\n             \"UsedCoupons\": \"已使用優惠卷\",\n             \"Transaction\": \"交易明細\",\n             \"BranchStore\": \"分店\",\n             \"Consuming\": \"消費金額\",\n             \"preConsuming\": \"消費前點數\",\n             \"TheNew\": \"本次新增\",\n             \"Accumulate\": \"累積點數\"\n    }\n\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "AuthTokenError Response",
          "content": "HTTP/1.1 200 Bad Request\n{\n    \"success\": false,\n    \"error\": \"Token error or expired\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi_20.php",
    "groupTitle": "Third_party"
  },
  {
    "type": "POST",
    "url": "GBapi_20/CheckAccount20",
    "title": "取得Account資訊 v2",
    "version": "0.2.0",
    "name": "CheckAccount20",
    "group": "Third_party",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>語系</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登入token</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "transations",
            "description": "<p>交易紀錄</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "transations_SROTE",
            "description": "<p>儲值紀錄</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "qryBonusEx",
            "description": "<p>點數</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "qryBonusEx_SROTE",
            "description": "<p>儲值</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": \"true\",\n    \"member_id\": \"70154\",\n    \"card_member\": \"0820000000446993\",\n    \"name\": \"GB test\",\n    \"qryBonusEx\": {\n         \"PermanentBonus\": \"2096\",\n         \"ExpiringPoint\": \"10000\",\n          \"QryBonusExList\": [\n            {\n              \"ExMonth\": \"2017-05\",\n              \"ExBonus\": \"10000\"\n            }\n          ]\n        },\n    \"qryBonusEx_SROTE\": {\n         \"PermanentBonus\": \"2096\",\n         \"ExpiringPoint\": \"10000\",\n          \"QryBonusExList\": [\n            {\n              \"ExMonth\": \"2017-05\",\n              \"ExBonus\": \"10000\"\n            }\n          ]\n        },\n     \"transations\": [\n          {\n            \"no\": 0,\n            \"date\": \"5/31/2017 2:31:18 PM\",\n            \"store\": \"TAIPEI HSINYI STORE\",\n            \"money\": \"2000\",\n            \"add_point\": \"0\",\n            \"point\": \"789\"\n          },\n          {\n            \"no\": 1,\n            \"date\": \"5/31/2017 2:31:18 PM\",\n            \"store\": \"TAIPEI HSINYI STORE\",\n            \"money\": \"3000\",\n            \"add_point\": \"0\",\n            \"point\": \"789\"\n          }, ...]\n     \"transations_store\": [\n          {\n            \"no\": 0,\n            \"date\": \"5/31/2017 2:31:18 PM\",\n            \"store\": \"TAIPEI HSINYI STORE\",\n            \"money\": \"2000\",\n            \"add_point\": \"20\",\n            \"checkNo\": \"11111111111\"\n          },\n          {\n            \"no\": 1,\n            \"date\": \"5/31/2017 2:31:18 PM\",\n            \"store\": \"TAIPEI HSINYI STORE\",\n            \"money\": \"10\",\n            \"add_point\": \"1\",\n            \"checkNo\": \"711111111112\"\n          }, ...]\n    \"sideTitle\": {\n             \"Title\": \"您好\",\n             \"topTitle\": \"帳戶總覽\",\n             \"topCardID\": \"iGB卡號\",\n             \"topPoint\": \"累積紅利點數\",\n             \"topRewards\": \"累積回饋金\",\n             \"Ecoupon\": \"優惠卷\",\n             \"SN\": \"序號\",\n             \"name\": \"名稱\",\n             \"Description\": \"說明\",\n             \"ExpiredDate\": \"截止日\",\n             \"UsedCoupons\": \"已使用優惠卷\",\n             \"Transaction\": \"交易明細\",\n             \"BranchStore\": \"分店\",\n             \"Consuming\": \"消費金額\",\n             \"preConsuming\": \"消費前點數\",\n             \"TheNew\": \"本次新增\",\n             \"Accumulate\": \"累積點數\"\n             \"PermanentPoint\": \"永久紅利點數\",\n             \"ExpiringPoint\": \"即將到期點數\",\n             \"ExpiringPointList\": \"即將到期點數明細\",\n             \"Time\": \"時間\",\n             \"Expired\": \"到期點數\",\n             \"storePoint\": \"儲值紀錄\",\n             \"storeAccumulate\": \"累積儲值金額\",\n             \"storeContent\": \"詳細內容\",\n             \"storeBonusDebit\": \"扣除儲值金\",\n             \"storeBonusCredit\": \"增加儲值金\",\n             \"storeFormNo\": \"單號\"\n         }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "AuthTokenError Response",
          "content": "HTTP/1.1 200 Bad Request\n{\n    \"success\": false,\n    \"error\": \"Token error or expired\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi_20.php",
    "groupTitle": "Third_party"
  },
  {
    "type": "POST",
    "url": "GBapi_20/MemberOpenCard",
    "title": "token_E-club會員開卡 (不用接)",
    "version": "0.1.0",
    "name": "MemberOpenCard",
    "group": "Third_party",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登入token</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cardID",
            "description": "<p>卡號</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "city_id",
            "description": "<p>城市</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>地址</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "memorialday",
            "description": "<p>紀念日(mm-dd)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tip",
            "description": "<p>備註</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "branch_store_id",
            "description": "<p>購買分店</p>"
          }
        ]
      }
    },
    "description": "<p>E-club會員開卡(飛訊: WBWM)。</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "true",
            "description": "<p>註冊成功。</p>"
          }
        ]
      }
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi_20.php",
    "groupTitle": "Third_party"
  },
  {
    "type": "POST",
    "url": "GBapi_20/OpenCardSendE",
    "title": "會員註冊+開卡 v3",
    "version": "0.1.0",
    "name": "OpenCardSendE",
    "group": "Third_party",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cardID",
            "description": "<p>卡號</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>名字</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "gender_id",
            "description": "<p>性別(改gender) 1男  2女</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>手機('唯一'帳號)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>使用者信箱('唯一'帳號)</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "city_id",
            "description": "<p>城市</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>地址</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>使用者密碼(RSA encrypt)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "birthday",
            "description": "<p>生日(yyyy-mm-dd)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "memorialday",
            "description": "<p>紀念日(mm-dd)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telephone",
            "description": "<p>電話</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tip",
            "description": "<p>備註</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "branch_store_id",
            "description": "<p>購買分店</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "message",
            "description": "<p>訂閱電子報</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": true,
            "field": "passCheck",
            "description": "<p>跳過開卡檢查 （網頁版）</p>"
          }
        ]
      }
    },
    "description": "<p>會員註冊+開卡(飛訊: WBWM)。</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "true",
            "description": "<p>註冊成功。</p>"
          }
        ]
      }
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi_20.php",
    "groupTitle": "Third_party"
  },
  {
    "type": "POST",
    "url": "GBapi_20/getStoreInfo",
    "title": "取得禮物卡資訊",
    "version": "0.2.0",
    "name": "getStoreInfo",
    "group": "Third_party",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>語系</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "card_member",
            "description": "<p>卡號</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "transations_SROTE",
            "description": "<p>儲值紀錄</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "qryBonusEx_SROTE",
            "description": "<p>儲值</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": \"true\",\n    \"card_member\": \"0820000000446993\",\n    \"qryBonusEx_SROTE\": {\n         \"PermanentBonus\": \"2096\",\n         \"ExpiringPoint\": \"10000\",\n          \"QryBonusExList\": [\n            {\n              \"ExMonth\": \"2017-05\",\n              \"ExBonus\": \"10000\"\n            }\n          ]\n        },\n     \"transations_store\": [\n          {\n            \"no\": 0,\n            \"date\": \"5/31/2017 2:31:18 PM\",\n            \"store\": \"TAIPEI HSINYI STORE\",\n            \"money\": \"2000\",\n            \"add_point\": \"20\",\n            \"checkNo\": \"11111111111\"\n          },\n          {\n            \"no\": 1,\n            \"date\": \"5/31/2017 2:31:18 PM\",\n            \"store\": \"TAIPEI HSINYI STORE\",\n            \"money\": \"10\",\n            \"add_point\": \"1\",\n            \"checkNo\": \"711111111112\"\n          }, ...]\n    \"sideTitle\": {\n             \"Title\": \"您好\",\n             \"topTitle\": \"帳戶總覽\",\n             \"topCardID\": \"iGB卡號\",\n             \"Time\": \"時間\",\n             \"storePoint\": \"儲值紀錄\",\n             \"storeAccumulate\": \"累積儲值金額\",\n             \"storeContent\": \"詳細內容\",\n             \"storeBonusDebit\": \"扣除儲值金\",\n             \"storeBonusCredit\": \"增加儲值金\",\n             \"storeFormNo\": \"單號\"\n         }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "AuthTokenError Response",
          "content": "HTTP/1.1 200 Bad Request\n{\n    \"success\": false,\n    \"error\": \"Not found user\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi_20.php",
    "groupTitle": "Third_party"
  },
  {
    "type": "POST",
    "url": "GBapi_20/getVouchers",
    "title": "取得優惠卷列表",
    "version": "0.2.0",
    "name": "getStoreInfo",
    "group": "Voucher",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>語系 (zh 中文, en 英文)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登入token</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "type",
            "description": "<p>usable 可用 / used 已使用/過期，不給此參數，預設是可使用的優惠卷</p>"
          }
        ]
      }
    },
    "description": "<p>取得優惠卷列表。</p>",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi_20.php",
    "groupTitle": "Voucher",
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": [\n        {\n            \"ticketId\": \"1908e696-e6da-40fc-aa52-b2b2b35c70e8\",  // 優惠卷ID\n            \"voucherId\": 1,\n            \"serialNumber\": 7,\n            \"cover\": \"https://inline.imgix.net/voucher-covers/--6cd15540-0d2a-45eb-80b1-e1396be1fbb6.png\",  // 優惠卷圖片\n            \"title\": \"問卷完成獎勵\",          // 優惠卷標題\n            \"content\": \"感謝您撥空填寫問卷\",  // 優惠卷內容\n            \"terms\": \"\",\n            \"providerId\": null,\n            \"usedAt\": null,\n            \"createdAt\": \"2018-04-25T06:04:35.902Z\",\n            \"expirationType\": \"never\",\n            \"availableAfter\": null,  // 有效期限開始\n            \"availableBefore\": null, // 有效期限結束\n            \"expiredAfter\": null,\n            \"usedReservationId\": null, \n            \"customerId\": \"-L4FVmhcsma9nXJT16iU\",\n            \"issuedBranchId\": \"-KpZYK_7r8Cymof4xnZO\",\n            \"usedBranchId\": null,\n            \"tag\": \"test01\"\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "AuthTokenError Response",
          "content": "HTTP/1.1 200 Bad Request\n{\n    \"success\": false,\n    \"error\": \"Token error or expired\"\n}",
          "type": "json"
        },
        {
          "title": "APIError Response",
          "content": "HTTP/1.1 200 Bad Request\n{\n    \"success\": false,\n    \"error\": \"input error, please check again\"\n}",
          "type": "json"
        },
        {
          "title": "InlinError Response",
          "content": "HTTP/1.1 200 Bad Request\n{\n    \"success\": false,\n    \"error\": \"inline error\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "POST",
    "url": "GBapi_20/useVoucher",
    "title": "核銷優惠券",
    "version": "0.2.0",
    "name": "useVoucher",
    "group": "Voucher",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登入token</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "ticketId",
            "description": "<p>優惠卷ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "branchId",
            "description": "<p>分店代號 inline_code</p>"
          }
        ]
      }
    },
    "description": "<p>核銷優惠券。</p>",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi_20.php",
    "groupTitle": "Voucher",
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": true,\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "AuthTokenError Response",
          "content": "HTTP/1.1 200 Bad Request\n{\n    \"success\": false,\n    \"error\": \"Token error or expired\"\n}",
          "type": "json"
        },
        {
          "title": "APIError Response",
          "content": "HTTP/1.1 200 Bad Request\n{\n    \"success\": false,\n    \"error\": \"input error, please check again\"\n}",
          "type": "json"
        },
        {
          "title": "InlinError Response",
          "content": "HTTP/1.1 200 Bad Request\n{\n    \"success\": false,\n    \"error\": \"inline error\"\n}",
          "type": "json"
        },
        {
          "title": "BranchIdError Response",
          "content": "HTTP/1.1 200 Bad Request\n{\n    \"success\": false,\n    \"error\": \"Not found branchId\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/apidoc/main.js",
    "group": "_home_ubuntu_www_icms_application_controllers_api_apidoc_main_js",
    "groupTitle": "_home_ubuntu_www_icms_application_controllers_api_apidoc_main_js",
    "name": ""
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/gb_api_doc_bak/main.js",
    "group": "_home_ubuntu_www_icms_application_controllers_api_gb_api_doc_bak_main_js",
    "groupTitle": "_home_ubuntu_www_icms_application_controllers_api_gb_api_doc_bak_main_js",
    "name": ""
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/gb_api_doc_m/main.js",
    "group": "_home_ubuntu_www_icms_application_controllers_api_gb_api_doc_m_main_js",
    "groupTitle": "_home_ubuntu_www_icms_application_controllers_api_gb_api_doc_m_main_js",
    "name": ""
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/gb_api_doc/main.js",
    "group": "_home_ubuntu_www_icms_application_controllers_api_gb_api_doc_main_js",
    "groupTitle": "_home_ubuntu_www_icms_application_controllers_api_gb_api_doc_main_js",
    "name": ""
  },
  {
    "type": "POST",
    "url": "GBapi/appRecommendBind",
    "title": "APP推薦",
    "version": "0.2.0",
    "name": "appRecommendBind",
    "group": "activity",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>推薦人手機</p>"
          }
        ]
      }
    },
    "description": "<p>APP推薦，5次即送。</p>",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "activity",
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": true,\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "APIError Response",
          "content": "HTTP/1.1 200 Bad Request\n{\n    \"success\": false,\n    \"error\": \"input error, please check again\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "POST",
    "url": "GBapi_20/appShardPost",
    "title": "APP分享貼文",
    "version": "0.2.0",
    "name": "appShardPost",
    "group": "activity",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "post_id",
            "description": "<p>FB發布成功後的Post_id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登入token</p>"
          }
        ]
      }
    },
    "description": "<p>APP分享貼文，10次即送。</p>",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi_20.php",
    "groupTitle": "activity",
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": true,\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "AuthTokenError Response",
          "content": "HTTP/1.1 200 Bad Request\n{\n    \"success\": false,\n    \"error\": \"Token error or expired\"\n}",
          "type": "json"
        },
        {
          "title": "APIError Response",
          "content": "HTTP/1.1 200 Bad Request\n{\n    \"success\": false,\n    \"error\": \"input error, please check again\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "POST",
    "url": "GBapi/Faq",
    "title": "取得iGB FAQ的所有內容",
    "version": "0.1.0",
    "name": "Faq",
    "group": "iGB",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>語系 (zh 中文, en 英文)</p>"
          }
        ]
      }
    },
    "description": "<p>取得iGB FAQ的所有內容。</p>",
    "filename": "/home/ubuntu/www/icms/application/controllers/api/GBapi.php",
    "groupTitle": "iGB",
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n\"code\": \"faq\",\n\"list\": [{\n    \t\"question\": \"►\\t如何加入GB E-Club獎勵會員？\",\n    \t\"answer\": \"加入 GB E-Club 獎勵會員有二種方式。透過GB官網或是GB APP線上免費申辦，於會員專區填寫GB E-Club獎勵會員資料，通過雙驗證後即可成為GB E-Club獎勵會員。爾後即享消費累積積分與優惠折抵等權益，相關優惠請詳E-Club獎勵會員權益說明。\"\n    \t},{\n    \t\"question\": \"►\\t加入 GB E-Club 需不需要費用？\",\n    \t\"answer\": \"不需要任何費用。加入GB E-Club之後您可以得到獎勵積分1000分並開始累積您的在GB的每一筆消費，當您在達到一定積分時就可以進行兌換與抵扣，您也會不定期收到GB的專屬行銷優惠活動資訊，讓您享受更多的驚喜與回饋！\"\n     }, ...\n   ]\n}",
          "type": "json"
        }
      ]
    }
  }
] });
